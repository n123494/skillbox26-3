#include "screen.hpp"
#include <iostream>
#include <string>

int main(int argc, char **argv)
{
	Screen screen;
    
    std::cout << "move      - move window\n";
    std::cout << "display   - show window\n";
    std::cout << "resize    - resize window\n";
    std::cout << "exit      - turn off screen\n";
    
    while (true)
    {
        std::string input;
        std::cout << "Enter command: ";
        std::cin >> input;
        
        if (input == "move")
        {
            screen.move();
        }
        else if (input == "display")
        {
            screen.display();
        }
        else if (input == "resize")
        {
            screen.resize();
        }
        else if (input == "exit")
        {
            break;
        }
        else
        {
            std::cout << "Invalid input\n";
        }
    }
    
	return 0;
}
