#pragma once
#include <iostream>

class Screen
{
    const unsigned x = 80;
    const unsigned y = 50;
    
    struct Window
    {
        std::pair<unsigned, unsigned> root;
        unsigned x = 20;
        unsigned y = 10;
        
        Window();
        
        void resize();
    };
    
    Window window;
    
public:
    Screen();
    ~Screen();
    
    void display() const;
    
    void move();
    
    void resize();
    
private:

    bool isWindow(const unsigned& x, const unsigned& y) const;

};

