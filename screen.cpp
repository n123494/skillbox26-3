#include "screen.hpp"

Screen::Screen()
{
}

Screen::~Screen()
{
}

Screen::Window::Window()
{
    root.first = 0;
    root.second = 0;
}

void Screen::display() const
{
    for (unsigned i = 0; i < y; ++i)
    {
        std::cout << std::endl;
        for (unsigned j = 0; j < x; ++j)
        {
            if (isWindow(j, i))
            {
                std::cout << "1";
            }
            else
            {
                std::cout << "0"; 
            }
        }
    }
    std::cout << std::endl;
}

void Screen::move()
{
    int offsetX, offsetY;
    std::cout << "Enter offset: ";
    std::cin >> offsetX >> offsetY;
    if (window.root.first + offsetX < 0 || window.root.first + offsetX >= x ||
        window.root.second + offsetY < 0 || window.root.second + offsetY >= y)
    {
        std::cout << "Out of screen\n";
        return;
    }
    window.root.first += offsetX;
    window.root.second += offsetY;
    std::cout << "Root (x: " << window.root.first << ", y: " << window.root.second << ")\n";
}

void Screen::Window::resize()
{
    std::cout << "Enter new window size: ";
    unsigned newx, newy;
    std::cin >> newx >> newy;
    x = newx;
    y = newy;
}

bool Screen::isWindow(const unsigned& x, const unsigned& y) const
{
    if (y >= window.root.second && y < window.root.second + window.y)
    {
        if (x >= window.root.first && x < window.root.first + window.x)
        {
            return true;
        }
    }
    return false;
}

void Screen::resize()
{
    window.resize();
}



